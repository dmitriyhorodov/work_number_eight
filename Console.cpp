#include "pch.h"
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <locale.h>

using namespace std;

int main() {
  setlocale(LC_ALL, "ru");
  int n, x, i;
  float Z, dob= 1;
  cout << "Введите переменную x и i\n";
  cin >> x;
  cin >> i;
  for (n = 1; n < 11; n++) {
	dob *= log10(i*x);
  }
  Z = sin((dob*180)/M_PI);
  cout << "\nПроизводная = " << Z;
  return 0;
} 